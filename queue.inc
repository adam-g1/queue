#if defined _queue_included
	#endinput
#endif
#define _queue_included

methodmap Queue < ArrayList
{
	/*
	 * Creates a new Queue
	 */
	public Queue()
	{
		return view_as<Queue>(new ArrayList());
	}
	/*
	 * Returns if the queue is empty
	 */
	property bool Empty
	{
		public get()
		{
			return this.Length == 0;
		}
	}
	/*
	 * Adds a client and a value to the queue.
	 * Does nothing if the client is already in the queue
	 */
	public void Insert(int iClient)
	{
		if (this.FindValue(iClient) == -1)
		{
			this.Push(iClient);
		}
	}
	/*
	 * Selects the first client in the queue, and then removes him from the queue
	 */
	public int Pop()
	{
		if(this.Length == 0)
		{
			return -1;
		}
		int iClient = this.Get(0)
		this.Erase(0);
		return iClient;
	}
	/*
	 * Drops a client from the queue if he exists in the queue
	 */
	public void Drop(int iClient)
	{
		int iIndex = this.FindValue(iClient, 0);
		if (iIndex != -1)
		{
			this.Erase(iIndex);
		}
	}
	/*
	 * Returns if a person is in the queue
	 */
	public bool InQueue(int iClient)
	{
		return this.FindValue(iClient) != -1;
	}
	/*
	 * Returns the position in the queue. The position starts at 0 and increases. If client not found, returns -1.
	 */
	public int GetPosition(int iClient)
	{
		return this.FindValue(iClient);
	}
	/*
	 * Returns the client at a specific index.
	 */
	public int GetClientAtPosition(int iPosition)
	{
		if(iPosition < 0 || iPosition >= this.Length)
		{
			LogError("Invalid position %d", iPosition);
			return -1;
		}
		return this.Get(iPosition, 0);
	}
}

methodmap PriorityQueue < Queue
{
	/*
	 * Creates a new Priority Queue.
	 */
	public PriorityQueue()
	{
		return view_as<PriorityQueue>(new ArrayList(2));
	}
	/*
	 * Returns the priority of a client in the queue
	 * returns -1.0 if not found
	 */
	public int GetPriority(int iClient)
	{
		int iIndex = this.FindValue(iClient);
		if(iIndex != -1)
		{
			return this.Get(iIndex, 1);
		}
		return -1;
	}
	/*
	 * Adds a player to the queue.
	 * If the player is already in the queue, his priority is updated.
	 */
	public void Insert(int iClient, int iPriority)
	{
		int iIndex = this.FindValue(iClient);
		
		if (iIndex != -1)
		{
			if(this.GetPriority(iClient) == iPriority)
			{
				return;
			}
			this.Erase(iIndex);
		}
		
		for(int i = 0; i < this.Length;i++)
		{
			if(this.GetPriority(this.GetClientAtPosition(i)) < iPriority)
			{
				this.ShiftUp(i);
				this.Set(i, iClient, 0);
				this.Set(i, iPriority, 1);
				return;
			}
		}
		this.Push(iClient);
		this.Set(this.Length - 1, iPriority, 1);
	}
} 