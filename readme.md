# Queue
***

## Description
***
The Queue is similar to a stack, but has access to dropping clients in the middle of the queue and peeking into the middle of the queue.
The PriorityQueue is similar to a normal queue, except that player's have an integer defining their priority in the queue. Players will be automatically pushed ahead of players with lower priority.

## Usage
***
Queues are created via `Queue yourQueue = new Queue()` and `PriorityQueue yourQueue = new PriorityQueue()`. All ArrayList methods are available, though you should use the Queue's methods for readability.
Make sure to use either `CloseHandle(<queue>)` or `delete <queue>`